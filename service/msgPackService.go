package service

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"math"
)

func Encode(data interface{}) ([]byte, error) {
	var buf bytes.Buffer
	err := encodeValue(&buf, data)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
func Decode(data []byte) (interface{}, error) {
	result, _ := decodeValue(data, 0)

	return result, nil
}

func encodeValue(buf *bytes.Buffer, val interface{}) error {
	switch v := val.(type) {
	case nil:
		buf.WriteByte(0xc0)
	case bool:
		if v {
			buf.WriteByte(0xc3)
		} else {
			buf.WriteByte(0xc2)
		}
	case int:
		if v >= 0 {
			if v <= 127 {
				buf.WriteByte(byte(v))
			} else {
				binary.Write(buf, binary.BigEndian, byte(0xcc))
				binary.Write(buf, binary.BigEndian, int8(v))
			}
		} else {
			if v >= -32 {
				buf.WriteByte(byte(v))
			} else if v >= -128 {
				binary.Write(buf, binary.BigEndian, byte(0xd0))
				binary.Write(buf, binary.BigEndian, int8(v))
			} else if v >= -32768 {
				binary.Write(buf, binary.BigEndian, byte(0xd1))
				binary.Write(buf, binary.BigEndian, int16(v))
			} else {
				binary.Write(buf, binary.BigEndian, byte(0xd2))
				binary.Write(buf, binary.BigEndian, int32(v))
			}
		}
	case uint:
		if v <= 127 {
			buf.WriteByte(byte(v))
		} else if v <= 255 {
			binary.Write(buf, binary.BigEndian, byte(0xcc))
			binary.Write(buf, binary.BigEndian, uint8(v))
		} else if v <= 65535 {
			binary.Write(buf, binary.BigEndian, byte(0xcd))
			binary.Write(buf, binary.BigEndian, uint16(v))
		} else {
			binary.Write(buf, binary.BigEndian, byte(0xce))
			binary.Write(buf, binary.BigEndian, uint32(v))
		}
	case float32:
		binary.Write(buf, binary.BigEndian, byte(0xca))
		binary.Write(buf, binary.BigEndian, v)
	case float64:
		binary.Write(buf, binary.BigEndian, byte(0xcb))
		binary.Write(buf, binary.BigEndian, v)
	case string:
		strBytes := []byte(v)
		strLen := len(strBytes)
		if strLen <= 31 {
			buf.WriteByte(byte(0xa0 + strLen))
		} else if strLen <= 255 {
			binary.Write(buf, binary.BigEndian, byte(0xd9))
			binary.Write(buf, binary.BigEndian, uint8(strLen))
		} else if strLen <= 65535 {
			binary.Write(buf, binary.BigEndian, byte(0xda))
			binary.Write(buf, binary.BigEndian, uint16(strLen))
		} else {
			binary.Write(buf, binary.BigEndian, byte(0xdb))
			binary.Write(buf, binary.BigEndian, uint32(strLen))
		}
		buf.Write(strBytes)
	case []interface{}:
		arrLen := len(v)
		if arrLen <= 15 {
			buf.WriteByte(byte(0x90 + arrLen))
		} else if arrLen <= 65535 {
			binary.Write(buf, binary.BigEndian, byte(0xdc))
			binary.Write(buf, binary.BigEndian, uint16(arrLen))
		} else {
			binary.Write(buf, binary.BigEndian, byte(0xdd))
			binary.Write(buf, binary.BigEndian, uint32(arrLen))
		}
		for _, elem := range v {
			err := encodeValue(buf, elem)
			if err != nil {
				return err
			}
		}
	case []byte:
		bytesLen := len(v)
		if bytesLen <= 255 {
			binary.Write(buf, binary.BigEndian, byte(0xc4))
			binary.Write(buf, binary.BigEndian, uint8(bytesLen))
		} else if bytesLen <= 65535 {
			binary.Write(buf, binary.BigEndian, byte(0xc5))
			binary.Write(buf, binary.BigEndian, uint16(bytesLen))
		} else {
			binary.Write(buf, binary.BigEndian, byte(0xc6))
			binary.Write(buf, binary.BigEndian, uint32(bytesLen))
		}
		buf.Write(v)
	case map[string]interface{}:
		mapLen := len(v)
		if mapLen <= 15 {
			buf.WriteByte(byte(0x80 + mapLen))
		} else if mapLen <= 65535 {
			binary.Write(buf, binary.BigEndian, byte(0xde))
			binary.Write(buf, binary.BigEndian, uint16(mapLen))
		} else {
			binary.Write(buf, binary.BigEndian, byte(0xdf))
			binary.Write(buf, binary.BigEndian, uint32(mapLen))
		}
		for key, val := range v {
			err := encodeValue(buf, key)
			if err != nil {
				return err
			}
			err = encodeValue(buf, val)
			if err != nil {
				return err
			}
		}
	default:
		return fmt.Errorf("unsupported type: %T", v)
	}
	return nil
}

func decodeValue(data []byte, pos int) (interface{}, int) {
	if len(data) <= pos {
		return nil, pos
	}
	switch data[pos] {
	case 0xc0:
		return nil, pos + 1
	case 0xc2:
		return false, pos + 1
	case 0xc3:
		return true, pos + 1
	case 0xcc:
		return int(data[pos+1]), pos + 2
	case 0xcd:
		return int(binary.BigEndian.Uint16(data[pos+1:])), pos + 3
	case 0xce:
		return int(binary.BigEndian.Uint32(data[pos+1:])), pos + 5
	case 0xcf:
		return int64(binary.BigEndian.Uint64(data[pos+1:])), pos + 9
	case 0xd0:
		return int8(data[pos+1]), pos + 2
	case 0xd1:
		return int16(binary.BigEndian.Uint16(data[pos+1:])), pos + 3
	case 0xd2:
		return int32(binary.BigEndian.Uint32(data[pos+1:])), pos + 5
	case 0xd3:
		return int64(binary.BigEndian.Uint64(data[pos+1:])), pos + 9
	case 0xca:
		return math.Float32frombits(binary.BigEndian.Uint32(data[pos+1:])), pos + 5
	case 0xcb:
		return math.Float64frombits(binary.BigEndian.Uint64(data[pos+1:])), pos + 9
	case 0xa0, 0xd9:
		strLen := int(data[pos] & 0x1f)
		return string(data[pos+1 : pos+1+strLen]), pos + 1 + strLen
	case 0xda:
		strLen := int(binary.BigEndian.Uint16(data[pos+1:]))
		return string(data[pos+3 : pos+3+strLen]), pos + 3 + strLen
	case 0xdb:
		strLen := int(binary.BigEndian.Uint32(data[pos+1:]))
		return string(data[pos+5 : pos+5+strLen]), pos + 5 + strLen
	case 0x90:
		arrLen := int(data[pos] & 0x0f)
		arr := make([]interface{}, arrLen)
		for i := 0; i < arrLen; i++ {
			val, newPos := decodeValue(data, pos+1)
			arr[i] = val
			pos = newPos
		}
		return arr, pos
	case 0xdc:
		arrLen := int(binary.BigEndian.Uint16(data[pos+1:]))
		arr := make([]interface{}, arrLen)
		pos += 3
		for i := 0; i < arrLen; i++ {
			val, newPos := decodeValue(data, pos)
			arr[i] = val
			pos = newPos
		}
		return arr, pos
	case 0xdd:
		arrLen := int(binary.BigEndian.Uint32(data[pos+1:]))
		arr := make([]interface{}, arrLen)
		pos += 5
		for i := 0; i < arrLen; i++ {
			val, newPos := decodeValue(data, pos)
			arr[i] = val
			pos = newPos
		}
		return arr, pos
	case 0x80:
		mapLen := int(data[pos] & 0x0f)
		m := make(map[string]interface{}, mapLen)
		pos++
		for i := 0; i < mapLen; i++ {
			key, newPos := decodeValue(data, pos)
			pos = newPos
			val, newPos := decodeValue(data, pos)
			pos = newPos
			m[key.(string)] = val
		}
		return m, pos
	case 0xde:
		mapLen := int(binary.BigEndian.Uint16(data[pos+1:]))
		m := make(map[string]interface{}, mapLen)
		pos += 3
		for i := 0; i < mapLen; i++ {
			key, newPos := decodeValue(data, pos)
			pos = newPos
			val, newPos := decodeValue(data, pos)
			pos = newPos
			m[key.(string)] = val
		}
		return m, pos
	case 0xdf:
		mapLen := int(binary.BigEndian.Uint32(data[pos+1:]))
		m := make(map[string]interface{}, mapLen)
		pos += 5
		for i := 0; i < mapLen; i++ {
			key, newPos := decodeValue(data, pos)
			pos = newPos
			val, newPos := decodeValue(data, pos)
			pos = newPos
			m[key.(string)] = val
		}
		return m, pos
	default:
		if data[pos] <= 0x7f {
			// Positive Fixnum
			return int(data[pos]), pos + 1
		} else if data[pos] <= 0x8f {
			// FixMap
			mapLen := int(data[pos] & 0x0f)
			m := make(map[string]interface{})
			pos++
			for i := 0; i < mapLen; i++ {
				key, newPos := decodeValue(data, pos)
				pos = newPos
				val, newPos := decodeValue(data, pos)
				pos = newPos
				m[key.(string)] = val
			}
			return m, pos
		} else if data[pos] <= 0x9f {
			// FixArray
			arrLen := int(data[pos] & 0x0f)
			arr := make([]interface{}, arrLen)
			pos++
			for i := 0; i < arrLen; i++ {
				val, newPos := decodeValue(data, pos)
				arr[i] = val
				pos = newPos
			}
			return arr, pos
		} else if data[pos] <= 0xbf {
			// FixStr
			strLen := int(data[pos] & 0x1f)
			return string(data[pos+1 : pos+1+strLen]), pos + 1 + strLen
		} else if data[pos] >= 0xe0 {
			// Negative Fixnum
			return int8(data[pos]), pos + 1
		} else {
			panic("Invalid data")
		}
	}
}
