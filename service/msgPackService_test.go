package service

import (
	"bytes"
	"reflect"
	"testing"
)

func TestEncode(t *testing.T) {
	tests := []struct {
		input  interface{}
		expect []byte
	}{
		{
			input:  nil,
			expect: []byte{0xc0},
		},
		{
			input:  42,
			expect: []byte{0x2a},
		},
		{
			input:  -1,
			expect: []byte{0xff},
		},
		{
			input:  3.14,
			expect: []byte{0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85, 0x1f},
		},
		{
			input:  true,
			expect: []byte{0xc3},
		},
		{
			input:  []interface{}{"hello", "world", 42},
			expect: []byte{0x93, 0xa5, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0xa5, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x2a},
		},
		{
			input:  map[string]interface{}{"foo": "bar", "baz": 42},
			expect: []byte{0x82, 0xa3, 0x66, 0x6f, 0x6f, 0xa3, 0x62, 0x61, 0x72, 0xa3, 0x62, 0x61, 0x7a, 0x2a},
		},
	}
	for _, tt := range tests {
		b, err := Encode(tt.input)
		if err != nil {
			t.Errorf("unexpected error: %v", err)
		}
		if !bytes.Equal(b, tt.expect) {
			t.Errorf("Encode(%v) = %v, expect %v", tt.input, b, tt.expect)
		}
	}
}

func TestDecode(t *testing.T) {
	tests := []struct {
		input  []byte
		expect interface{}
	}{
		{
			input:  []byte{0xc0},
			expect: nil,
		},
		{
			input:  []byte{0x2a},
			expect: 42,
		},
		{
			input:  []byte{0xff},
			expect: int8(-1),
		},
		{
			input:  []byte{0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85, 0x1f},
			expect: 3.14,
		},
		{
			input:  []byte{0xc3},
			expect: true,
		},
		{
			input:  []byte{0x93, 0xa5, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0xa5, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x2a},
			expect: []interface{}{"hello", "world", 42},
		},
		{
			input:  []byte{0x82, 0xa3, 0x66, 0x6f, 0x6f, 0xa3, 0x62, 0x61, 0x72, 0xa3, 0x62, 0x61, 0x7a, 0x2a},
			expect: map[string]interface{}{"foo": "bar", "baz": 42},
		},
	}

	for _, test := range tests {
		result, err := Decode(test.input)
		if err != nil {
			t.Errorf("Error decoding JSON: %v", err)
		}
		if !reflect.DeepEqual(result, test.expect) {
			t.Errorf("DecodeJson(%v) = %v, expected %v", test.input, result, test.expect)
		}
	}
}
