# MsgPack
在不依靠msgpack第三方套件的前提，完成MessagePack、Json兩格式間的資料轉換

## 目的
基於Gin框架的HTTP API server，提供兩個API，/v1/encodeToMsgPack和/v1/decodeMsgPack，分別用於將JSON資料轉換成MsgPack格式，以及將MsgPack資料轉換成JSON格式。在實現中使用了encoding/binary和encoding/hex等標準庫，並定義了service包中的Encode和Decode函數，用於編解碼。

## 專案需求說明
* You can use any programming language you want.
* Using 3rd-party msgpack library is forbidden.
* You need to provide a github or gitlab repository or gist page link to us.
* Add a readme file in the repo to describe how to execute your program.
* Unit Test (need to provide README.md)

## 方案檔資料夾說明範例

### service
* service 資料夾專門放MessagePack編解譯功能，以及測試檔案


## 系統環境

* go 1.19
* gin-gonic/gin v1.9.0


## 建置作業
1. pull 本專案
2. 安裝go，並於本專案目錄執行

``` 
go run main.go
```
3. 可見系統建立API，並參考API文件(msgPackAPIDoc.json)使用


## Unit test 使用說明
1. 進入service資料夾
2. 執行
``` 
go test
```
3. 執行後，測試報告將會顯示在終端機中。
### Test Cases
* The following data types are tested:

1. Nil
2. Integer
3. Float
4. Boolean
5. Interface
6. Map