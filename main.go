package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"msgPack/service"
	"net/http"
)

func main() {

	r := gin.Default()
	v1 := r.Group("/v1")

	v1.POST("/encodeToMsgPack", encodeToMsgPack)
	v1.POST("/decodeMsgPack", decodeMsgPack)

	err := r.Run(":8000")
	if err != nil {
		return
	}
}

func encodeToMsgPack(c *gin.Context) {
	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "bad request"})
		return
	}
	var data map[string]interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Println("json unmarshal error : ", err.Error())
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid request body"})
		return
	}

	mpData, err := service.Encode(data)
	if err != nil {
		fmt.Println("encode error : ", err.Error())
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "encode error" + err.Error()})
		return
	}
	//fmt.Println(mpData)
	hexStr := hex.EncodeToString(mpData)
	c.Header("Content-Type", "application/x-msgpack")
	c.String(http.StatusOK, hexStr)
	//c.String(http.StatusOK, string(mpData))
}

func decodeMsgPack(c *gin.Context) {
	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "bad request"})
		return
	}
	fmt.Println(body)
	mpData, err := service.Decode(body)
	if err != nil {
		fmt.Println("decode error : ", err.Error())
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "decode error" + err.Error()})
		return
	}

	c.JSON(http.StatusOK, mpData)
}
